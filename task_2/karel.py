from textx import metamodel_from_file
from karel_robot.run import *

class KarelInterpreter(object):

    def process_command(self, s):
        command_type = s.__class__.__name__
        method_name = f'process_{command_type.lower()}'
        method = getattr(self, method_name, None)
        if method is not None:
            method(s)
        else:
            print(f"Unknown command type: {command_type}")

    def process_turn(self, s):
        if s.where == 'left':
            turn_left()
        if s.where == 'right':
            turn_right()

    def process_move(self, s):
        move()

    def process_exit(self, s):
        exit()

    def process_str(self, s):
        if s == "move":
            self.process_move(s)
        elif s == "exit":
            self.process_exit(s)
        else:
            print(f"Unknown string command: {s}")

    def process_beeper(self, s):
        if s.action == 'pick':
            pick_beeper()
        if s.action == 'put':
            put_beeper()

    def process_statements(self, cs):
        for c in cs:
            self.process_statement(c)

    def process_statement(self, s):
        self.process_command(s)

    def interpret(self, model):
        for s in model.commands:
            self.process_statement(s)

    def process_statementif(self, s):
        if self.evaluate_expression(s.cond):
            for command in s.commands:
                self.process_statement(command)
        elif s.else_commands:  # проверка наличия блока else
            for command in s.else_commands:
                self.process_statement(command)

    def process_statementwhile(self, s):
        while self.evaluate_expression(s.cond):
            for command in s.commands:
                self.process_statement(command)

    def evaluate_expression(self, expr):
        if isinstance(expr, str):  # Добавлено для обработки строковых литералов
            return self.evaluate_check(expr)
        expr_type = expr.__class__.__name__.lower()
        method_name = f'evaluate_{expr_type}'
        method = getattr(self, method_name, None)
        if method is not None:
            return method(expr)
        else:
            print(f"Unknown expression type: {expr_type}")
            return False

    def evaluate_not(self, expr):
        return not self.evaluate_expression(expr.expr)

    def evaluate_check(self, expr):
        if expr == 'front_is_treasure':
            return front_is_treasure()
        if expr == 'front_is_blocked':
            return front_is_blocked()
        return False

    def evaluate_beepercheck(self, expr):
        return beeper_is_present()

    def evaluate_dir(self, expr):
        if expr == 'north':
            return facing_north()
        if expr == 'south':
            return facing_south()
        if expr == 'west':
            return facing_west()
        if expr == 'east':
            return facing_east()
        return False

    def evaluate_or(self, expr):
        return self.evaluate_expression(expr.left) or self.evaluate_expression(expr.right)

    def evaluate_and(self, expr):
        return self.evaluate_expression(expr.left) and self.evaluate_expression(expr.right)

if __name__ == "__main__":
    robot_mm = metamodel_from_file('karel.tx')
    robot_model = robot_mm.model_from_file("maze.karel")

    interpreter = KarelInterpreter()
    interpreter.interpret(robot_model)
