from textx import metamodel_from_file
from karel_robot.run import *

class KarelInterpreter(object):

    def process_command(self, s):
        # Определение типа команды и вызов соответствующего метода
        command_type = s.__class__.__name__
        method_name = f'process_{command_type.lower()}'
        method = getattr(self, method_name, None)
        if method is not None:
            method(s)
        else:
            print(f"Unknown command type: {command_type}")

    def process_turn(self, s):
        if s.where == 'left':
            turn_left()
        if s.where == 'right':
            turn_right()

    def process_move(self, s):
        move()

    def process_exit(self, s):
        exit()

    def process_str(self, s):
        if s == "move":
            self.process_move(s)
        elif s == "exit":
            self.process_exit(s)
        else:
            print(f"Unknown string command: {s}")

    def process_beeper(self, s):
        if s.action == 'pick':
            pick_beeper()
        if s.action == 'put':
            put_beeper()

    def process_statements(self, cs):
        for c in cs:
            self.process_statement(c)

    def process_statement(self, s):
        self.process_command(s)

    def interpret(self, model):
        for s in model.commands:
            self.process_statement(s)

if __name__ == "__main__":
    robot_mm = metamodel_from_file('karel-plain.tx')
    robot_model = robot_mm.model_from_file("maze-plain.karel")

    interpreter = KarelInterpreter()
    interpreter.interpret(robot_model)
