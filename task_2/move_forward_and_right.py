from karel_robot.run import *

def move_forward_and_right():
    # Move 2 steps forward
    for _ in range(2):
        move()
    # Turn right
    turn_right()
    # Move 2 steps to the right
    for _ in range(2):
        move()

# Run the program
move_forward_and_right()
