from textx import metamodel_from_file
from karel_robot.run import *
import sys

class ReturnException(Exception):
    def __init__(self, value):
        self.value = value

class BreakException(Exception):
    pass

class KarelInterpreter(object):

    def __init__(self):
        self.call_stack = []  # стек вызовов
        self.context = {}  # текущий контекст (переменные и их значения)
        self.datatypes = {} # хранение типов данных для переменных
        self.functions = {}  # словарь функций
        self.subprograms = {}  # словарь подпрограмм

    def process_function(self, f):
        self.functions[f.name] = f

    def process_subprogram(self, sp):
        self.subprograms[sp.name] = sp

    def process_call(self, c):
        # Получаем подпрограмму или функцию
        if c.name in self.subprograms:
            sp_or_func = self.subprograms[c.name]
        elif c.name in self.functions:
            sp_or_func = self.functions[c.name]
        else:
            raise ValueError(f"Unknown function/subprogram {c.name}")

        # Сохраняем текущий контекст и добавляем его в стек
        self.call_stack.append(self.context)
        self.context = {}

        print(f"Before for param_name", sp_or_func.params, c.args, flush=True)

        # Передаем параметры
        for param_name, arg in zip(sp_or_func.params, c.args):
            print(f"In for param_name", flush=True)
            value = self.evaluate_expression(arg)
            self.context[param_name] = value
            # Добавить тип данных для переменной
            self.datatypes[param_name] = type(value).__name__

        # Выполняем команды
        try:
            for command in sp_or_func.commands:
                self.process_statement(command)
        except ReturnException as ret:
            print(f"Function {c.name} returned value:", ret.value, flush=True)
            return ret.value
        finally:
            # Восстанавливаем предыдущий контекст
            self.context = self.call_stack.pop()

    def evaluate_call(self, expr):
        print("Before function call, context:", self.context, flush=True)
        result = self.process_call(expr)
        print("After function call, context:", self.context, flush=True)
        return result

    def process_returncommand(self, r):
        print(f"Returncommand{r.value}", flush=True)
        value = self.evaluate_expression(r.value)
        print(f"Returncommand{value}", flush=True)
        # Можно использовать исключение для возвращения из функции и передачи значения
        raise ReturnException(value)

    def process_break(self, b):
        raise BreakException

    def process_command(self, s):
        command_type = s.__class__.__name__
        method_name = f'process_{command_type.lower()}'
        method = getattr(self, method_name, None)
        if method is not None:
            method(s)
        else:
            print(f"Unknown command type: {command_type}")

    def process_turn(self, s):
        if s.where == 'left':
            turn_left()
            print(f"Turned left", flush=True)
        if s.where == 'right':
            turn_right()
            print(f"Turned right", flush=True)

    def process_move(self, s):
        move()
        print(f"Moved forward", flush=True)

    def process_exit(self, s):
        exit()

    def process_str(self, s):
        if s == "move":
            self.process_move(s)
        elif s == "exit":
            self.process_exit(s)
        else:
            print(f"Unknown string command: {s}")

    def process_beeper(self, s):
        if s.action == 'pick':
            pick_beeper()
        if s.action == 'put':
            put_beeper()

    def process_statements(self, cs):
        for c in cs:
            self.process_statement(c)

    def process_statement(self, s):
        self.process_command(s)

    def interpret(self, model):
        for f in getattr(model, 'functions', []):
            self.process_function(f)
        for sp in getattr(model, 'subprograms', []):
            self.process_subprogram(sp)
        for s in model.commands:
            self.process_statement(s)

    def process_statementif(self, s):
        if self.evaluate_expression(s.cond):
            for command in s.commands:
                self.process_statement(command)
        elif s.else_commands:  # проверка наличия блока else
            for command in s.else_commands:
                self.process_statement(command)

    def process_statementwhile(self, s):
        while self.evaluate_expression(s.cond):
            try:
                for command in s.commands:
                    self.process_statement(command)
            except BreakException:
                break

    def evaluate_booleanliteral(self, expr):
        print(f"Evaluating boolean literal: {expr.value}", flush=True)
        if expr.value == 'true':
            return True
        if expr.value == 'false':
            return False
        return False

    def evaluate_true(self, expr):
        return True

    def evaluate_false(self, expr):
        return False

    def evaluate_expression(self, expr):
        if isinstance(expr, str):  # Добавлено для обработки строковых литералов
            return self.evaluate_check(expr)
        expr_type = expr.__class__.__name__.lower()
        print(f"Function {expr_type}", flush=True)
        if expr_type == "TrueLiteral":  # используйте правильное имя типа, если это отличается
            print(f"Function {expr_type} returned value:", self.evaluate_true(expr), flush=True)
            return self.evaluate_true(expr)
        elif expr_type == "FalseLiteral":  # используйте правильное имя типа, если это отличается
            print(f"Function {expr_type} returned value:", self.evaluate_false(expr), flush=True)
            return self.evaluate_false(expr)
        method_name = f'evaluate_{expr_type}'
        method = getattr(self, method_name, None)
        if method is not None:
            return method(expr)
        else:
            print(f"Unknown expression type: {expr_type}")
            return False

    def evaluate_not(self, expr):
        return not self.evaluate_expression(expr.expr)

    def evaluate_check(self, expr):
        if expr == 'front_is_treasure':
            return front_is_treasure()
        if expr == 'front_is_blocked':
            return front_is_blocked()
        if expr == 'true':
            return True
        if expr == 'false':
            return False
        return False

    def evaluate_beepercheck(self, expr):
        return beeper_is_present()

    def evaluate_dir(self, expr):
        if expr == 'north':
            return facing_north()
        if expr == 'south':
            return facing_south()
        if expr == 'west':
            return facing_west()
        if expr == 'east':
            return facing_east()
        return False

    def evaluate_or(self, expr):
        return self.evaluate_expression(expr.left) or self.evaluate_expression(expr.right)

    def evaluate_and(self, expr):
        return self.evaluate_expression(expr.left) and self.evaluate_expression(expr.right)

if __name__ == "__main__":
    with open('output.txt', 'w') as f:
        # Перенаправить стандартный вывод в файл
        original_stdout = sys.stdout  # Сохраните оригинальный stdout
        sys.stdout = f

        robot_mm = metamodel_from_file('karel_ultimate.tx')
        robot_model = robot_mm.model_from_file("maze_ultimate.karel")

        interpreter = KarelInterpreter()
        interpreter.interpret(robot_model)

        f.flush()

    # Возвращаем стандартный вывод обратно
    sys.stdout = original_stdout

