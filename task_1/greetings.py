from textx import metamodel_from_file

class GreetingInterpreter:
    def interpret(self, model):
        for greeting in model.greetings:
            print(f"Hello {greeting.entity}!")

if __name__ == "__main__":
    greeting_mm = metamodel_from_file('greetings.tx')
    greeting_model = greeting_mm.model_from_file("greetings_example.greet")

    interpreter = GreetingInterpreter()
    interpreter.interpret(greeting_model)
